#k_corvuria
##d_bal_dostan
###c_arca_corvur
441 = {		#Arca Corvur

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_bal_dostan_01
		special_building = castanorian_citadel_bal_dostan_01
	}
}
2220 = {

    # Misc
    holding = none

    # History

}
2221 = {	#Village of Corvuria

    # Misc
    holding = city_holding

    # History
}
2222 = {

    # Misc
    holding = church_holding

    # History

}

###c_mihaelas_redoubt
440 = {		#Mihaela's Redoubt

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2223 = {

    # Misc
    holding = church_holding

    # History

}
2224 = {

    # Misc
    holding = none

    # History

}

###c_elderwright
439 = {		#Elderwright

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2225 = {

    # Misc
    holding = city_holding

    # History

}
2226 = {

    # Misc
    holding = none

    # History

}
2227 = {

    # Misc
    holding = none

    # History

}

###c_silcorvur
438 = {		#Silcorvur

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
2228 = {

    # Misc
    holding = none

    # History

}

##d_blackwoods
###c_blackwoods
435 = {		#Blackwoods

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
2212 = {

    # Misc
    holding = none

    # History

}
2213 = {

    # Misc
    holding = none

    # History

}

###c_kortir
437 = {		#Kortir

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
2217 = {

    # Misc
    holding = none

    # History

}
2218 = {

    # Misc
    holding = none

    # History

}
2219 = {

    # Misc
    holding = city_holding

    # History

}

###c_karns_hold
436 = {		#Karn's Hold

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
2214 = {

    # Misc
    holding = none

    # History

}
2215 = {

    # Misc
    holding = church_holding

    # History

}
2216 = {

    # Misc
    holding = none

    # History

}

###c_holstead
2211 = {		#Holstead

    # Misc
    culture = korbarid
    religion = korbarid_dragon_cult
	holding = castle_holding

    # History
}
429 = {

    # Misc
    holding = city_holding

    # History

}
2210 = {

    # Misc
    holding = none

    # History

}

##d_tiferben
###c_aelaintaire
426 = {		#Aelaintaire

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2187 = {

    # Misc
    holding = church_holding

    # History

}
2188 = {

    # Misc
    holding = none

    # History

}
2189 = {

    # Misc
    holding = city_holding

    # History

}

###c_rotwall
423 = {		#Rotwall

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2182 = {

    # Misc
    holding = none

    # History

}

###c_marchfield
425 = {		#Marchfield

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2183 = {

    # Misc
    holding = church_holding

    # History

}
2184 = {

    # Misc
    holding = none

    # History

}

###c_ioans_ford
427 = {		#Ioan's Ford

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2185 = {

    # Misc
    holding = city_holding

    # History

}
2186 = {

    # Misc
    holding = none

    # History

}

###c_cannvalley
430 = {		#Cannvalley

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2192 = {

    # Misc
    holding = church_holding

    # History

}
2193 = {

    # Misc
    holding = none

    # History

}
2195 = {

    # Misc
    holding = city_holding

    # History

}

###c_rackmans_court
428 = {		#Rackman's Court

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2190 = {

    # Misc
    holding = church_holding

    # History

}
2191 = {

    # Misc
    holding = none

    # History

}

##d_ravenhill
###c_ravenhill
431 = {		#Ravenhill

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2196 = {

    # Misc
    holding = city_holding

    # History

}
2197 = {

    # Misc
    holding = none

    # History

}

###c_cannmarionn
432 = {		#Cannmarionn

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2198 = {

    # Misc
    holding = city_holding

    # History

}
2199 = {

    # Misc
    holding = church_holding

    # History

}
2201 = {

    # Misc
    holding = none

    # History

}
2202 = {

    # Misc
    holding = none

    # History

}

###c_gablaine
433 = {		#Gablaine

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2203 = {

    # Misc
    holding = church_holding

    # History

}
2204 = {

    # Misc
    holding = city_holding

    # History

}
2205 = {

    # Misc
    holding = castle_holding

    # History

}
2206 = {

    # Misc
    holding = none

    # History

}

###c_arca_kaldere
434 = {		#Arca Kaldere

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2207 = {

    # Misc
    holding = church_holding

    # History

}
2208 = {

    # Misc
    holding = none

    # History

}
2209 = {

    # Misc
    holding = none

    # History

}
