#k_carneter
##d_carneter
###c_carneter
19 = {		#Carneter

    # Misc
    culture = carnetori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1058 = {	#Moonwatch

    # Misc
	holding = none

    # History

}
1011 = {	#Rubenwic

    # Misc
	holding = city_holding

    # History

}

###c_woodwell
22 = {		#Woodwell

    # Misc
    culture = carnetori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1010 = {	#Thorngate

    # Misc
	holding = none

    # History

}
1057 = {	#Bowyers Creek

    # Misc
	holding = church_holding

    # History

}

###c_ancards_crossing
30 = {		#Ancard's Crossing

    # Misc
    culture = carnetori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1051 = {	#Greenwood

    # Misc
	holding = none

    # History

}

###c_timberfort
57 = {		#Timberfort

    # Misc
    culture = carnetori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1148 = {

    # Misc
	holding = none

    # History

}


###c_ilvandet
31 = {		#Ilvandet

    # Misc
    culture = carnetori
    religion = cult_of_the_dame
    holding = castle_holding

    # History

}
1060 = {	#Verenath

    # Misc
	holding = none

    # History

}
1059 = {	#Tederpuille

    # Misc
	holding = none

    # History

}
20 = {		#Ordoin

    # Misc
	holding = city_holding

    # History

}