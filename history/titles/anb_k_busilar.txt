k_busilar = {
	1000.1.1 = {
		change_development_level = 8
	}
	1014.1.1 = {
		liege = e_bulwar
		holder = sun_elvish0040 # Thranduir Tirenzuir
	}
}

c_busilar = {
	1000.1.1 = {
		change_development_level = 10
	}
}

c_lioncost = {
	1000.1.1 = {
		change_development_level = 9
	}
}

c_derancestir = {
	800.1.1 = {
		liege = "k_busilar"
	}
	1000.1.1 = {
		change_development_level = 9
	}
	1019.1.1 = {
		holder = 599	#Deran of Derancestir; placeholder date for the acquiral of the region
	}
	
}

c_lions_pass = {
	1000.1.1 = {
		change_development_level = 9
	}
}

c_port_jaher = {
	1000.1.1 = {
		change_development_level = 13
	}
}

d_hapaine = {
	1000.1.1 = {
		change_development_level = 10
	}
}

c_hapaine = {
	1000.1.1 = {
		change_development_level = 11
	}
}

d_lorin = {
	1000.1.1 = {
		change_development_level = 7
	}
}

d_canno = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_khenahap = {
	1000.1.1 = {
		change_development_level = 6
	}
}

d_hortosare = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_hortosare = {
	1000.1.1 = {
		change_development_level = 6
	}
}

d_stonehold = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_stonehold = {
	1000.1.1 = {
		change_development_level = 6
	}
}