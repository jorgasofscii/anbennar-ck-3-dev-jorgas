﻿d_minar_temple = {
	1000.1.1 = {
		liege = k_lorent
		holder = 170
	}
}

e_castanor = {
	-640.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_I # Castan I the Progenitor
	}
	-621.1.1 = { # right year, random date PLACEHOLDER
		holder = 0 # Castanorian Interregnum
	}
	-613.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_II # Castan II Beastbane
	}
	-569.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_III # Castan III Dwarf-friend
	}
	-521.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_IV # Castan IV Realmbuilder
	}
	-471.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_V # Castan V the Great
	}
	-420.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_VI # Castan VI Giantsbane
	}
	-357.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_VII # Castan VII the Preserver
	}
	-307.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_VIII # Castan VIII
	}
	-302.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_IX # Castan IX Peacemaker
	}
	-294.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_X # Castan X the Quareller
	}
	-248.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XI # Castan XI the Righteous
	}
	-244.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XII # Castan XII the Old
	}
	-144.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XIII # Castan XIII
	}
	-132.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XIV # Castan XIV
	}
	-111.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XV # Castan XV the Short
	}
	-111.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XVI # Castan XVI
	}
	-83.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XVII # Castan XVII
	}
	-70.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XVIII # Castan XVIII
	}
	-59.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XIX # Castan XIX
	}
	-33.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XX # Castan XX the Youthful
	}
	-30.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXI # Castan XXI the Compassionate
	}
	-27.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXII # Castan XXII the Eccentric
	}
	-26.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXIII # Castan XXIII
	}
	-13.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXIV # Castan XXIV the Unready
	}
	4.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXV # Castan XXV the Perished
	}
	5.1.1 = { # right year, random date PLACEHOLDER
		holder = 0 # no one 
	}
	31.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXVI # Castan XXVI the Unwavering
	}
	43.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXVII # Castan XXVII the Sickly
	}
	101.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXVIII # Castan XXVIII
	}
	118.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXIX # Castan XXIX
	}
	157.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXX # Castan XXX
	}
	201.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXI # Castan XXXI the Venerable
	}
	307.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXII # Castan XXXII
	}
	323.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXIII # Castan XXXIII
	}
	374.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXIV # Castan XXXIV
	}
	398.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXV # Castan XXXV the Defiant
	}
	474.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXVI # Castan XXXVI the Dragonfriend
	}
	500.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXVII # Castan XXXVII the Rebuilder
	}
	518.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXVIII # Castan XXXVIII the Humble
	}
	566.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXIX # Castan XXXIX the Silent
	}
	666.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XL # Castan XL the Steward
	}
	669.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLI # Castan XLI the Vengeful
	}
	700.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLII # Castan XLII the Cowardly
	}
	707.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLIII # Castan XLIII
	}
	728.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLIV # Castan XLIV
	}
	761.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLV # Castan XLV
	}
	793.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLVI # Castan XLVI
	}
	809.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLVII # Castan XLVII Frostbane
	}
	859.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLVIII # Castan XLVIII
	}
	865.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLIX # Castan XLIX Ebonfrost
	}
	893.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_L # Castan L
	}
	895.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_LI # Castan LI
	}
	936.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_LII # Castan LII the Kind
	}
	978.1.5 = {
		holder = castan_LIII # Castan LIII the Enthralled
	}
	1015.12.14 = {
		holder = 33 # Nichmer the Sorcerer-King
	}
	1020.10.31 = {	#Battle of Trialmount
		holder = 0
	}
}

d_knights_of_the_saltmarch = {
	20.1.1 = {
	}
	1020.1.1 = {
	}
	1020.1.1 = {
		liege = "k_verne"
	}
	1020.1.1 = {
		holder = 179
	}
}

d_skaldskola = {
	1003 = {
		holder = 300012
	}
}