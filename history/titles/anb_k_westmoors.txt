k_westmoors = {
	1000.1.1 = { change_development_level = 6 }
}

d_beronmoor = {
	990.4.6 = {
		holder = 57 #Garther Beron
	}
}

c_moorton = {
	1000.1.1 = { change_development_level = 7 }
}

c_heathcliff = {
	1000.1.1 = { change_development_level = 5 }
}

c_cockerwall = {
	1000.1.1 = { change_development_level = 5 }
}

d_moorhills = {
	1008.7.4 = {
		holder = 58 #Deris Fouler
	}
}

c_redferne_hill = {
	1000.1.1 = { change_development_level = 5 }
}

c_woldenmarck = {
	1000.1.1 = { change_development_level = 5 }
}

d_westmoor = {
	1003.3.2 = {
		holder = 59 #Marlen Cottersea
	}
}

c_brontay_point = {
	1000.1.1 = { change_development_level = 7 }
}

c_cottersea = {
	1000.1.1 = { change_development_level = 5 }
}

c_leapdon = {
	1000.1.1 = { change_development_level = 5 }
}