﻿60000 = { # Ida "the Mad"
	name = "Ida"
	dynasty = dynasty_beldarbronhd
	religion = castanorian_pantheon
	culture = marcher
	female = yes
	
	trait = race_human
	trait = vengeful
	trait = arbitrary
	trait = arrogant
	trait = disfigured
	trait = magical_affinity_2
	trait = education_martial_4
	
	967.6.17 = {
		birth = yes
	}
	
	1021.07.03 = {
		give_nickname = nick_the_accursed
	}
}

60001 = { # Lantsida the Chosen
	name = "Lantsida"
	dynasty = dynasty_beldarbronhd
	religion = castanorian_pantheon
	culture = ryalani
	female = yes
	
	trait = race_human
	trait = ambitious
	trait = deceitful
	trait = arrogant
	trait = beauty_bad_2
	trait = magical_affinity_1 # Not as strong as her "mentor" but she's sneakier
	trait = education_intrigue_4
	
	996.6.30 = {
		birth = yes
	}
	
	1022.1.1 = {
		set_mother = character:60000 # "Adopted" by Ida
	}
}

60002 = { # Willichar of Guidesway
	name = "Willichar"
	dynasty = dynasty_nurcedor
	religion = cult_of_the_dame
	culture = businori
	
	trait = race_human
	trait = arrogant
	trait = callous
	trait = zealous
	trait = education_martial_4
	
	1002.11.21 = {
		birth = yes
	}
}

60003 = { # Rabac of Dostan’s Way
	name = "Rabac"
	dynasty = dynasty_wayguard
	religion = castanorian_pantheon
	culture = marcher
	
	trait = race_human
	trait = just
	trait = diligent
	trait = zealous
	trait = education_martial_1
	
	993.7.8 = {
		birth = yes
	}
	
	1010.4.30 = {
		add_spouse = 60004
	}
	
	1021.11.31 = {
		give_nickname = nick_the_liberator
	}
}

60004 = { # Rabac Wife
	name = "Sophia"
	religion = castanorian_pantheon
	culture = marcher
	female = yes
	
	trait = race_human
	
	995.12.1 = {
		birth = yes
	}
	
	988.3.14 = {
		add_spouse = 60003
	}
}

60005 = { # Bellac the old
	name = "Bellac"
	dynasty = dynasty_devaced
	religion = castanorian_pantheon
	culture = marcher
	
	trait = race_human
	trait = calm
	trait = stubborn
	trait = forgiving
	trait = education_diplomacy_2
	
	970.6.6 = {
		birth = yes
	}
	
	988.3.14 = {
		add_spouse = 60006
	}
	
	1021.11.31 = {
		give_nickname = nick_the_old
		#These tiles were conquered from him while he was in Anbenncost so he has claims
		add_pressed_claim = title:c_diremill
		add_pressed_claim = title:c_catelsvord
	}
}

60006 = { # Bellac Wife
	name = "Bellac"
	dynasty = dynasty_devaced
	religion = castanorian_pantheon
	culture = marcher
	female = yes
	
	trait = race_human
	trait = calm
	trait = stubborn
	trait = forgiving
	trait = education_diplomacy_2
	
	977.7.8 = {
		birth = yes
	}
	
	988.3.14 = {
		add_spouse = 60005
	}
}

60007 = { # Child 1
	name = "Christine"
	dynasty = dynasty_devaced
	religion = castanorian_pantheon
	culture = marcher
	female = yes
	
	father = 60005
	mother = 60006
	
	# ;)
	980.6.8 = {
		birth = yes
	}
}

60008 = { # Child 2
	name = "Bellac"
	dynasty = dynasty_devaced
	religion = castanorian_pantheon
	culture = marcher
	
	father = 60005
	mother = 60006
	
	982.5.22 = {
		birth = yes
	}
}

60009 = { # Child 3
	name = "Hulderic"
	dynasty = dynasty_devaced
	religion = castanorian_pantheon
	culture = marcher
	
	father = 60005
	mother = 60006
	
	983.7.18 = {
		birth = yes
	}
}

60010 = { # Child 4
	name = "Bertrada"
	dynasty = dynasty_devaced
	religion = castanorian_pantheon
	culture = marcher
	female = yes
	
	father = 60005
	mother = 60006
	
	985.1.10 = {
		birth = yes
	}
}

60011 = {
	name = "Rogec"
	#TODO
	#dynasty = 
	religion = castanorian_pantheon
	#TODO
	culture = black_castanorian
	
	trait = race_human
	trait = arrogant
	trait = wrathful
	trait = sadistic
	trait = scarred
	trait = education_martial_4
	
	989.10.28 = {
		birth = yes
		effect = {
			set_variable = {
				name = false_convert
				value = faith:cult_of_agrados
			}
			add_piety = 300 # So he doesn't start at -200
		}
	}
}

60012 = {
	name = "Osnath"
	#TODO
	#dynasty = 
	religion = castanorian_pantheon
	#TODO
	culture = black_castanorian
	
	trait = race_human
	trait = paranoid
	trait = calm
	trait = deceitful
	trait = shrewd
	trait = education_intrigue_2
	
	986.1.7 = {
		birth = yes
		effect = {
			set_variable = {
				name = false_convert
				value = faith:cult_of_agrados
			}
		}
	}
}

60013 = {
	name = "Ulfric"
	#TODO
	#dynasty = 
	religion = castanorian_pantheon
	culture = black_castanorian
	
	trait = race_human
	trait = zealous
	trait = ambitious
	trait = chaste
	trait = education_stewardship_1
	
	1000.5.2 = {
		birth = yes
	}
}

60014 = {
	name = "Godryc"
	dynasty = dynasty_acengard
	religion = castanorian_pantheon
	culture = adeanic
	
	mother = 60016
	father = 60017
	
	trait = race_human
	trait = impatient
	trait = greedy
	trait = cynical
	trait = education_diplomacy_2
	
	998.6.4 = {
		birth = yes
	}
}

60015 = {
	name = "Lisban"
	dynasty = dynasty_acengard
	religion = castanorian_pantheon # TODO - Add event for her to join the cult
	culture = adeanic
	female = yes
	
	mother = 60016
	father = 60017
	
	trait = race_human
	trait = impatient
	trait = greedy
	trait = cynical
	trait = education_diplomacy_2
	
	995.4.21 = {
		birth = yes
		add_pressed_claim = title:d_cannwood
	}
}

60016 = {
	name = "Lisban"
	dynasty = dynasty_acengard
	religion = castanorian_pantheon
	culture = black_castanorian
	female = yes
	
	trait = race_human
	
	975.2.18 = {
		birth = yes
	}
}

60017 = {
	name = "Cecill"
	dynasty = dynasty_acengard
	religion = castanorian_pantheon
	culture = adeanic
	
	trait = race_human
	
	975.2.18 = {
		birth = yes
	}
}

#####ALDANMORE

61001 = {
	name = "Osric"
	dna = 61001_osric_of_aldenmore
	dynasty = dynasty_aldenmore #Aldenmore
	religion = "castanorian_pantheon"
	culture = "castanorian"

	diplomacy = 10
	martial = 6
	stewardship = 12
	intrigue = 7
	learning = 5
	prowess = 6

	trait = education_stewardship_3
	trait = greedy
	trait = ambitious
	trait = gregarious
	trait = race_human
	
	965.6.21 = {
		birth = yes
	}

	995.4.7 = {
		add_spouse = 61002
	}
	
}

61002 = {
	name = "Emma"
	religion = "castanorian_pantheon"
	culture = "castanorian"
	female = yes

	trait = education_diplomacy_3
	trait = gregarious
	trait = patient
	trait = sadistic
	
	trait = race_human
	
	973.9.11 = {
		birth = yes
	}
	
}

61003 = {
	name = "Caylen"
	dynasty = dynasty_aldenmore #Aldenmore
	religion = "castanorian_pantheon"
	culture = "castanorian"
	mother = 61002	#Emma
	father = 61001	#Osric

	trait = education_martial_2
	trait = arbitrary
	trait = brave
	trait = arrogant
	trait = blademaster_1
	trait = rakish
	trait = drunkard
	trait = athletic
	trait = race_human
	
	996.6.11 = {
		birth = yes
	}
	
}

61004 = {
	name = "Auci"
	dynasty = dynasty_aldenmore #Aldenmore
	religion = "castanorian_pantheon"
	culture = "castanorian"
	female = yes
	mother = 61002	#Emma
	father = 61001	#Osric

	trait = education_diplomacy_2
	trait = gregarious
	trait = compassionate
	trait = stubborn
	
	trait = race_human
	
	998.8.3 = {
		birth = yes
	}
	
}

61005 = {
	name = "Ekbert"
	dynasty = dynasty_aldenmore #Aldenmore
	religion = "castanorian_pantheon"
	culture = "castanorian"
	mother = 61002	#Emma
	father = 61001	#Osric

	trait = education_stewardship_3
	trait = content
	trait = honest
	trait = stubborn
	
	trait = race_human
	
	1000.3.25 = {
		birth = yes
	}
	
	1003.3.25 = {
		give_nickname = nick_the_spare
	}
	
}

61006 = {
	name = "Alaric"
	dynasty = dynasty_ryalfeld #Ryalfeld
	religion = "castanorian_pantheon"
	culture = "castanorian"
	father = 61007	#Tomac

	trait = education_martial_2
	trait = stubborn
	trait = vengeful
	trait = brave
	
	trait = race_human
	
	995.12.7 = {
		birth = yes
	}
	
	1019.3.11 = {
		add_pressed_claim = title:c_aldenmore
	}

	1021.11.31 = {
		employer = 89	#placeholder in absence of an accurate date
	}
}

61007 = {
	name = "Tomac"
	dynasty = dynasty_ryalfeld #Ryalfeld
	religion = "castanorian_pantheon"
	culture = "castanorian"

	trait = education_martial_2
	trait = gallant
	trait = just
	trait = content
	trait = temperate
	
	trait = race_human
	
	964.2.13 = {
		birth = yes
	}

	1019.3.11 = {
		death = "1019.3.11"
	}
}

escanni_0001 = {
    name = "Clotaire"
    dynasty = dynasty_sheldgard
    religion = "castanorian_pantheon"
    culture = "castanorian"

    trait = education_martial_3
    trait = brave
    trait = gregarious
    trait = compassionate
    trait = athletic
    trait = loyal
	
	982.3.13 = {
		birth = yes
	}
	
	1010.2.7 = {
		effect = {
			set_relation_friend = character:90
		}
		add_spouse = escanni_0002
	}
}

escanni_0002 = {
    name = "Edit"
    dynasty = dynasty_anor
    religion = "castanorian_pantheon"
    culture = "castanorian"
	female = yes
	
    trait = education_stewardship_2
    trait = temperate
    trait = gregarious
    trait = compassionate
    trait = administrator
    trait = race_human
    987.1.1 = {
        birth = yes
    }

    1010.2.7 = {
		effect = {
			set_relation_soulmate = character:escanni_0001
		}
		add_spouse = escanni_0001
    }
}

escanni_0003 = {
    name = "Lain"
    dynasty = dynasty_sheldgard
    religion = "castanorian_pantheon"
    culture = "castanorian"

	#TODO these are weird in game
    trait = brave
    trait = wrathful
    trait = charming
    trait = intellect_good_1
	
      father = escanni_0001
	mother = escanni_0002
	
    1010.11.9 = {
        birth = yes
    }
}


escanni_0004 = {
    name = "Rican"
    dynasty = dynasty_eswall
    religion = "castanorian_pantheon"
    culture = "castanorian"

    trait = education_intrigue_2
    trait = callous
    trait = ambitious
    trait = deceitful
    trait = race_human
	
    999.9.4 = {
        birth = yes
    }
	
	1020.1.1 = {
		effect = {
			add_pressed_claim = title:d_ardent_glade
			add_pressed_claim = title:c_cadells_rest
			add_pressed_claim = title:c_grenmore
			add_pressed_claim = title:c_ardent_keep
		}
	}
}

escanni_0005 = {
    name = "arnulf"
    dynasty = dynasty_andhol
    religion = "cult_of_esmaryal"
    culture = "old_esmari"

    trait = education_martial_2 
    trait = greedy
    trait = gregarious
    trait = brave
    trait = adventurer
    trait = race_human 
	
      father = escanni_0008
	
    993.03.11 = {
        birth = yes
    }
}

escanni_0006 = {
    name = "Ulric"
    dynasty = dynasty_andhol
    religion = "cult_of_esmaryal"
    culture = "old_esmari"

    trait = education_martial_2 
    trait = zealous
    trait = diligent
    trait = paranoid
    trait = adventurer
    trait = race_human 
	
      father = escanni_0008

    995.06.19 = {
        birth = yes
    }
}

escanni_0007 = {
    name = "stovan"
    dynasty = dynasty_hagstow
    religion = "castanorian_pantheon"
    culture = "castanorian"

    trait = education_martial_2 
    trait = just
    trait = compassionate
    trait = arrogant
    trait = hunter
    trait = athletic
    trait = race_human
	
    993.03.11 = {
        birth = yes
		effect = {
				give_witch_secret_or_trait_effect = yes
    }
	
}

escanni_0008 = {
    name = "ulfric"
    dynasty = dynasty_andhol
    religion = "cult_of_esmaryal"
    culture = "old_esmari"

    trait = education_learning_3
    trait = gregarious
    trait = arrogant
    trait = just
    trait = race_human
	
    955.04.05 = {
        birth = yes
    }
1017.09.22 = {
		death = "1017.09.22"
	}
}
