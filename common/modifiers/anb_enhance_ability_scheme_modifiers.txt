﻿enhance_ability_working_harder = {
    icon = treatment_positive
	scheme_power = 10
	scheme_success_chance = 10
}
enhance_ability_working_target_pushes_working_harder = {
    icon = treatment_positive
	scheme_power = 15
	scheme_success_chance = 10
}
