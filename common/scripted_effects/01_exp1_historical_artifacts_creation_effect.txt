﻿# Historical Artifacts Creation #

set_artifact_rarity_common = {
	save_scope_value_as = {
		name = quality
		value = 15
	}
	save_scope_value_as = {
		name = wealth
		value = 15
	}
}

set_artifact_rarity_masterwork = {
	save_scope_value_as = {
		name = quality
		value = 30
	}
	save_scope_value_as = {
		name = wealth
		value = 30
	}
}

set_artifact_rarity_famed = {
	save_scope_value_as = {
		name = quality
		value = 70
	}
	save_scope_value_as = {
		name = wealth
		value = 70
	}
}

set_artifact_rarity_illustrious = {
	save_scope_value_as = {
		name = quality
		value = 100
	}
	save_scope_value_as = {
		name = wealth
		value = 100
	}
}

create_artifact_pedestal_christian_relic_effect_hist = { }

create_artifact_pedestal_islamic_relic_effect_hist = { }

create_artifact_pedestal_buddhism_relic_effect_hist = { }

create_artifact_pedestal_branch_relic_zoroastr_effect = { }

create_artifact_pedestal_branch_relic_germanic_effect = { }

create_artifact_pedestal_branch_relic_slavic_effect = { }

create_artifact_pedestal_branch_relic_boog_effect = { }

create_artifact_pedestal_branch_relic_hinduism_effect = { }

create_artifact_pedestal_branch_relic_general_effect = {
	# Get the character the artifact is being made for.
	$OWNER$ = { save_scope_as = owner }
	set_artifact_rarity_illustrious = yes

	save_scope_value_as = {
		name = branch_name
		value = flag:general
	}

	# Create the artifact
	create_artifact = {	
		name = artifact_pedestal_branch_name
		description = artifact_pedestal_branch_description
		type = pedestal
		template = branch_general_template
		visuals = pedestal_branch_relic_hinduism
		wealth = scope:wealth
		quality = scope:quality
		history = {
			type = created_before_history
		}
		modifier = artifact_placeholder_modifier
		save_scope_as = newly_created_artifact
	}

	scope:newly_created_artifact = {
		set_variable = {	#This is needed to determine what religion the branch will work for.
			name = relic_religion
			value = $RELIGION$
		}
		set_variable = { name = historical_unique_artifact value = yes }
		add_scaled_artifact_modifier_piety_effect = yes
		add_scaled_artifact_modifier_grandeur_small_effect = yes
		if = {
			limit = {
				OR = {
					rarity = famed
					rarity = illustrious
				}
			}
			add_2_scaled_artifact_modifier_devotion_effect = yes
			
		}
		else = {
			add_scaled_artifact_modifier_devotion_effect = yes
		}
		save_scope_as = epic
	}
}

#Common Artifacts

create_artifact_excalibur_effect = {
	# Get the character the artifact is being made for.
	$OWNER$ = { save_scope_as = owner }
	# Not really used, but if we don't set the scopes we get errors in the feature selection
	set_artifact_rarity_common = yes

	# Create the artifact
	create_artifact = {
		name = excalibur_name
		description = excalibur_description
		type = pedestal
		visuals = excalibur
		wealth = scope:wealth
		quality = scope:quality
		history = {
			type = created_before_history
		}
		modifier = excalibur_modifier
		save_scope_as = newly_created_artifact
		decaying = no
	}

	scope:newly_created_artifact = {
		set_variable = { name = historical_unique_artifact value = yes }
		set_variable = excalibur
		save_scope_as = epic
	}
}

create_artifact_edmund_head_effect = {
	# Get the character the artifact is being made for.
	$OWNER$ = { save_scope_as = owner }
	# Not really used, but if we don't set the scopes we get errors in the feature selection
	set_artifact_rarity_illustrious = yes

	# Create the artifact
	create_artifact = {	
		name = edmund_head_name
		description = edmund_head_description
		type = pedestal
		visuals = head
		wealth = scope:wealth
		quality = scope:quality
		template = christian_relic_template
		history = {
			type = created
			date = 869.11.20
			recipient = character:163064 #Eadmund the Martyr
			location = province:1520 #Blything
		}
		modifier = edmund_head_modifier
		save_scope_as = newly_created_artifact
		decaying = no
	}

	scope:newly_created_artifact = {
		set_variable = { name = historical_unique_artifact value = yes }
		set_variable = edmund_head
		add_scaled_artifact_modifier_devotion_effect = yes
		set_variable = {
			name = relic
			value = flag:christian
		}
		save_scope_as = epic
		add_artifact_history = {
			type = given
			date = 946.5.27
			recipient = character:33350 # Æthelstan
		}
		add_artifact_title_history = {
			target = title:k_england
			date = 955.11.24
		}
	}
}

create_artifact_makarakundala_effect = {
	# Get the character the artifact is being made for.
	$OWNER$ = { save_scope_as = owner }
	# Not really used, but if we don't set the scopes we get errors in the feature selection

	set_artifact_rarity_famed = yes

	# Create the artifact
	create_artifact = {	
		name = artifact_makarakundala_name
		description = artifact_makarakundala_description
		type = pedestal
		visuals = riches
		wealth = scope:wealth
		quality = scope:quality
		template = general_unique_template
		history = {
			type = created_before_history
		}
		modifier = makarakundala_modifier
		save_scope_as = newly_created_artifact
	}

	scope:newly_created_artifact = {
		set_variable = { name = historical_unique_artifact value = yes }
		set_variable = makarakundala
		add_scaled_artifact_modifier_devotion_effect = yes
		save_scope_as = epic
	}
}

#Masterwork Artifacts

create_artifact_ibeji_effect = { }

create_artifact_chinese_caligraphy_effect = { }

create_artifact_throne_charlemagne_effect = { }

create_artifact_throne_solomon_effect = { }

create_artifact_throne_scone_effect = { }

create_artifact_wall_banner_kaviani_effect = { }

create_artifact_sculpture_cabinet_pentapyrgion_effect = { }

create_artifact_monomachus_crown_effect = { }

create_artifact_ruyi_effect = { }

create_artifact_jewelled_danda_effect = { }

create_artifact_khanda_effect = { }

create_artifact_dagger_of_rostam_effect = { }

create_artifact_ascalon_effect = { }

create_artifact_zomorrodnegar_effect = { }

create_artifact_kaves_apron_effect = { }

create_artifact_muhammads_epistles_effect = { }

create_artifact_aruval_effect = { }

create_artifact_navaratna_effect = { }

create_artifact_pedestal_ikenga_effect = { }

create_artifact_afarganyu_effect = { }

create_artifact_siddhachakra_effect = { }

create_artifact_staff_kakusandha_effect = { }

create_artifact_konagamana_effect = { }

create_artifact_robe_kassapa_effect = { }

#Famed Artifacts

create_artifact_reichskrone_effect = { }

create_artifact_cintamani_hindu_effect = { }

create_artifact_cintamani_buddhist_effect = { }

create_artifact_wall_banner_edessa_effect = { }

create_artifact_wall_cid_sword_effect = { }

create_artifact_wall_muhammad_sword_effect = { }

create_artifact_sword_mmaagha_kamalu_effect = { }

create_artifact_bronze_head_effect = { }

create_artifact_crystal_carving_effect = { }

create_artifact_wall_sword_attila_effect = { }

create_artifact_pedestal_great_diamond_effect = { }

create_artifact_pedestal_al_jabal_effect = { }

create_artifact_pedestal_al_yatima_effect = { }

create_artifact_pedestal_cup_jamshid_effect = { }

create_artifact_pedestal_crown_iron_effect = { }

create_artifact_nikephoros_crown_effect = { }

create_artifact_pedestal_shankha_conch_effect = { }

create_artifact_skull_cap_charlemagne_effect = { }

create_artifact_essen_crown_effect = { }

create_artifact_dhammapada_effect = { }

create_artifact_vinaya_pitaka_effect = { }

create_artifact_sutta_pitaka_effect = { }

create_artifact_abhidhamma_pitaka_effect = { }

create_artifact_qadib_al_mulk_effect = { }

create_artifact_al_dawat_effect = { }

create_artifact_al_hafir_effect = { }

create_artifact_al_sayf_al_khass_effect = { }

create_artifact_durendal_effect = { }

create_artifact_nagelring_effect = { }

create_artifact_szczerbiec_effect = { }

create_artifact_kladenets_effect = { }

create_artifact_legbiter_effect = { }

create_artifact_quernbiter_effect = { }

create_artifact_dragvandil_effect = { }

create_artifact_curtana_effect = { }

create_colada_effect = { }

create_artifact_angelicas_ring_effect = { }

create_artifact_olifant_effect = { }

create_artifact_aram_effect = { }

create_artifact_sledovik_effect = { }

create_artifact_kantele_effect = { }

#Illustrious Artifacts

create_artifact_pedestal_david_harp_effect = { }

create_artifact_joyeuse_effect = { }

create_artifact_papal_tiara_effect = { }

create_artifact_sculpture_ark_of_covenant_effect = { }

create_artifact_pedestal_koh_i_noor_effect = { }

create_artifact_al_taj_crown_effect = { }

create_artifact_wall_banner_thankfulness_effect = { }

create_artifact_arms_of_alexander_effect = { }

create_artifact_turquoise_throne_effect = { }

create_artifact_peacock_throne_effect = { }

create_artifact_spear_of_the_prophet_effect = { }

create_artifact_mantle_of_the_prophet_effect = { }

create_artifact_sculpture_babr_e_bayan_effect = { }

create_artifact_pedestal_justinian_effect = { }
