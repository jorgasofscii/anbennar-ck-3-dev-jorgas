﻿bulwari_sun_cults_religion = {
	family = rf_bulwari
	graphical_faith = dharmic_gfx
	doctrine = sun_cult_hostility_doctrine

	#Main Group
	doctrine = doctrine_no_head	
	doctrine = doctrine_gender_male_dominated
	doctrine = doctrine_pluralism_righteous
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_polygamy
	doctrine = doctrine_divorce_approval
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_consanguinity_cousins

	#Crimes
	doctrine = doctrine_homosexuality_crime
	doctrine = doctrine_adultery_men_crime
	doctrine = doctrine_adultery_women_crime
	doctrine = doctrine_kinslaying_close_kin_crime
	doctrine = doctrine_deviancy_crime
	doctrine = doctrine_witchcraft_crime

	#Clerical Functions
	doctrine = doctrine_clerical_function_taxation
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	
	traits = {
		virtues = {
			honest
			calm
			compassionate
			generous
			lifestyle_reveler
		}
		sins = {
			wrathful
			callous
			deceitful
			greedy
		}
	}

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {	#TODO
		{ name = "holy_order_jaherian_exemplars" }
		{ name = "holy_order_faith_maharatas" }
	}

	holy_order_maa = { praetorian }	#their men at arms

	localization = {
		#HighGod - Surael
		HighGodName = bulwari_sun_cults_high_god_name
		HighGodNamePossessive = bulwari_sun_cults_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HERHIS_HIS
		HighGodHerHis = CHARACTER_HERHIM_HIM
		HighGodNameAlternate = bulwari_sun_cults_high_god_name_alternate
		HighGodNameAlternatePossessive = bulwari_sun_cults_high_god_name_alternate_possessive

		#Creator
		CreatorName = bulwari_sun_cults_creator_god_name
		CreatorNamePossessive = bulwari_sun_cults_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_SHE
		CreatorHerHis = CHARACTER_HERHIS_HER
		CreatorHerHim = CHARACTER_HERHIM_HER

		#HealthGod
		HealthGodName = bulwari_sun_cults_health_god_name
		HealthGodNamePossessive = bulwari_sun_cults_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_HE
		HealthGodHerHis = CHARACTER_HERHIS_HIS
		HealthGodHerHim = CHARACTER_HERHIM_HIM
		
		#FertilityGod
		FertilityGodName = bulwari_sun_cults_fertility_god_name
		FertilityGodNamePossessive = bulwari_sun_cults_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_HE
		FertilityGodHerHis = CHARACTER_HERHIS_HIS
		FertilityGodHerHim = CHARACTER_HERHIM_HIM

		#WealthGod
		WealthGodName = bulwari_sun_cults_wealth_god_name
		WealthGodNamePossessive = bulwari_sun_cults_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = bulwari_sun_cults_household_god_name
		HouseholdGodNamePossessive = bulwari_sun_cults_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_HE
		HouseholdGodHerHis = CHARACTER_HERHIS_HIS
		HouseholdGodHerHim = CHARACTER_HERHIM_HIM

		#FateGod
		FateGodName = bulwari_sun_cults_fate_god_name
		FateGodNamePossessive = bulwari_sun_cults_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_IT
		FateGodHerHis = CHARACTER_HERHIS_ITS
		FateGodHerHim = CHARACTER_HERHIM_IT

		#KnowledgeGod
		KnowledgeGodName = bulwari_sun_cults_knowledge_god_name
		KnowledgeGodNamePossessive = bulwari_sun_cults_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod
		WarGodName = bulwari_sun_cults_war_god_name
		WarGodNamePossessive = bulwari_sun_cults_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod
		TricksterGodName = bulwari_sun_cults_trickster_god_name
		TricksterGodNamePossessive = bulwari_sun_cults_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = bulwari_sun_cults_night_god_name
		NightGodNamePossessive = bulwari_sun_cults_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod
		WaterGodName = bulwari_sun_cults_water_god_name
		WaterGodNamePossessive = bulwari_sun_cults_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_HE
		WaterGodHerHis = CHARACTER_HERHIS_HIS
		WaterGodHerHim = CHARACTER_HERHIM_HIM


		PantheonTerm = bulwari_sun_cults_high_god_name
		PantheonTermHasHave = pantheon_term_has
		GoodGodNames = {
			bulwari_sun_cults_high_god_name
			bulwari_sun_cults_high_god_name_alternate
			bulwari_sun_cults_lord_of_light
		}
		
		DevilName = bulwari_sun_cults_devil_name
		DevilNamePossessive = bulwari_sun_cults_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_IT
		DevilHerHis = CHARACTER_HERHIS_ITS
		DevilHerselfHimself = bulwari_sun_cults_devil_herselfhimself
		EvilGodNames = {
			bulwari_sun_cults_devil_name
			bulwari_sun_cults_evil_god_malevolent_dark
			bulwari_sun_cults_evil_god_rasa
			bulwari_sun_cults_evil_god_jiqaat
			bulwari_sun_cults_evil_god_ukedu
			bulwari_sun_cults_evil_god_darkness_evils
		}
		HouseOfWorship = bulwari_sun_cults_house_of_worship
		HouseOfWorshipPlural = bulwari_sun_cults_house_of_worship_plural
		ReligiousSymbol = bulwari_sun_cults_religious_symbol
		ReligiousText = bulwari_sun_cults_religious_text
		ReligiousHeadName = bulwari_sun_cults_religious_head_title
		ReligiousHeadTitleName = bulwari_sun_cults_religious_head_title_name
		DevoteeMale = bulwari_sun_cults_devotee_male
		DevoteeMalePlural = bulwari_sun_cults_devotee_male_plural
		DevoteeFemale = bulwari_sun_cults_devotee_female
		DevoteeFemalePlural = bulwari_sun_cults_devotee_female_plural
		DevoteeNeuter = bulwari_sun_cults_devotee_neuter
		DevoteeNeuterPlural = bulwari_sun_cults_devotee_neuter_plural
		PriestMale = bulwari_sun_cults_priest
		PriestMalePlural = bulwari_sun_cults_priest_plural
		PriestFemale = bulwari_sun_cults_priest
		PriestFemalePlural = bulwari_sun_cults_priest_plural
		PriestNeuter = bulwari_sun_cults_priest
		PriestNeuterPlural = bulwari_sun_cults_priest_plural
		AltPriestTermPlural = bulwari_sun_cults_priest_term_plural
		BishopMale = bulwari_sun_cults_bishop
		BishopMalePlural = bulwari_sun_cults_bishop_plural
		BishopFemale = bulwari_sun_cults_bishop
		BishopFemalePlural = bulwari_sun_cults_bishop_plural
		BishopNeuter = bulwari_sun_cults_bishop
		BishopNeuterPlural = bulwari_sun_cults_bishop_plural
		DivineRealm = bulwari_sun_cults_positive_afterlife
		PositiveAfterLife = bulwari_sun_cults_positive_afterlife
		NegativeAfterLife = bulwari_sun_cults_negative_afterlife
		DeathDeityName = bulwari_sun_cults_death_name
		DeathDeityNamePossessive = bulwari_sun_cults_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		WitchGodName = bulwari_sun_cults_trickster_god_name
		WitchGodHerHis = CHARACTER_HERHIS_HIS
		WitchGodSheHe = CHARACTER_SHEHE_HE
		WitchGodHerHim = CHARACTER_HERHIM_HIM
		WitchGodMistressMaster = Master
		WitchGodMotherFather = Father

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		bulwari_sun_cult = {
			color = { 255 201 14 }
			icon = bulwari_sun_cult
			
			holy_site = bulwar
			holy_site = azkaszelazka

			doctrine = tenet_unrelenting_faith
			doctrine = tenet_ritual_hospitality
			doctrine = tenet_suraels_rebirth
			
			#Main Group
			doctrine = doctrine_temporal_head # Jaher at start but if possible it should be switched to no one if he dies and the sun elves fail to stabilise
			doctrine = doctrine_gender_equal
			doctrine = doctrine_theocracy_lay_clergy

			#Clerical Functions
			doctrine = doctrine_clerical_function_taxation
			doctrine = doctrine_clerical_succession_temporal_fixed_appointment
		}

		old_bulwari_sun_cult = { 
			color = { 255 67 24 }
			icon = old_bulwari_sun_cult

			holy_site = bulwar
			holy_site = eduz_vacyn
			holy_site = azka_sur
			holy_site = azkaszelazka
			holy_site = azka_szel_udam

			doctrine = tenet_communal_identity
			doctrine = tenet_sun_worship
			doctrine = tenet_false_conversion_sanction

			doctrine = doctrine_clerical_function_alms_and_pacification
			doctrine = doctrine_clerical_succession_spiritual_fixed_appointment

			localization = {
				#HighGod - Surakel
				HighGodName = old_bulwari_sun_cult_high_god_name
				HighGodNamePossessive = old_bulwari_sun_cult_high_god_name_possessive
		
				#HealthGod
				HealthGodName = old_bulwari_sun_cult_health_god_name
				HealthGodNamePossessive = old_bulwari_sun_cult_health_god_name_possessive
				
				#FertilityGod
				FertilityGodName = old_bulwari_sun_cult_fertility_god_name
				FertilityGodNamePossessive = old_bulwari_sun_cult_fertility_god_name_possessive
				#WealthGod
				WealthGodName = old_bulwari_sun_cult_wealth_god_name
				WealthGodNamePossessive = old_bulwari_sun_cult_wealth_god_name_possessive
		
				#HouseholdGod
				HouseholdGodName = old_bulwari_sun_cult_household_god_name
				HouseholdGodNamePossessive = old_bulwari_sun_cult_household_god_name_possessive
		
				#FateGod
				FateGodName = old_bulwari_sun_cult_fate_god_name
				FateGodNamePossessive = old_bulwari_sun_cult_fate_god_name_possessive
		
				#KnowledgeGod
				KnowledgeGodName = old_bulwari_sun_cult_knowledge_god_name
				KnowledgeGodNamePossessive = old_bulwari_sun_cult_knowledge_god_name_possessive
		
				#WarGod
				WarGodName = old_bulwari_sun_cult_war_god_name
				WarGodNamePossessive = old_bulwari_sun_cult_war_god_name_possessive
		
				#TricksterGod
				TricksterGodName = old_bulwari_sun_cult_trickster_god_name
				TricksterGodNamePossessive = old_bulwari_sun_cult_trickster_god_name_possessive
		
				#WaterGod
				WaterGodName = old_bulwari_sun_cult_water_god_name
				WaterGodNamePossessive = old_bulwari_sun_cult_water_god_name_possessive
		
		
				PantheonTerm = old_bulwari_sun_cult_high_god_name
				PantheonTermHasHave = pantheon_term_has
				GoodGodNames = {
					old_bulwari_sun_cult_high_god_name
					bulwari_sun_cults_high_god_name_alternate
					bulwari_sun_cults_lord_of_light
				}
				
				ReligiousSymbol = old_bulwari_sun_cult_religious_symbol
				ReligiousText = old_bulwari_sun_cult_religious_text
				WitchGodName = old_bulwari_sun_cult_trickster_god_name
		
				GHWName = ghw_purification
				GHWNamePlural = ghw_purifications
			}
		}
		
		cult_of_the_eclipse = {
			color = { 255 201 14 }
			icon = the_dame
			
			doctrine = tenet_false_conversion_sanction
		}
		
		moon_cult = {
			color = { 255 201 14 }
			icon = the_dame
		}
	}
}