﻿#All Anbennar marriage triggers
anb_marriage_trigger = {
	compatible_race_non_mule_trigger = {
		ACTOR = $ACTOR$
		RECIPIENT = $RECIPIENT$
	}
	elf_duke_marriage_trigger = {
		ACTOR = $ACTOR$
		RECIPIENT = $RECIPIENT$
	}
	elf_succession_marriage_trigger = {
		ACTOR = $ACTOR$
		RECIPIENT = $RECIPIENT$
	}
	sun_elf_marriage_trigger = {
		ACTOR = $ACTOR$
		RECIPIENT = $RECIPIENT$
	}
}

#Two characters can have non-mule children
#For use with marriage/concubinage
compatible_race_non_mule_trigger = {
	OR = {
		#Elf-human-orc group
		AND = {
			$ACTOR$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
				}
			}
			$RECIPIENT$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
					has_trait = race_harpy
				}
			}
		}
		#Dwarf
		AND = {
			$ACTOR$ = {
				has_trait = race_dwarf
			}
			$RECIPIENT$ = {
				has_trait = race_dwarf
			}
		}
		#Halfling
		AND = {
			$ACTOR$ = {
				has_trait = race_halfling
			}
			$RECIPIENT$ = {
				has_trait = race_halfling
			}
		}
		#Gnome
		AND = {
			$ACTOR$ = {
				has_trait = race_gnome
			}
			$RECIPIENT$ = {
				has_trait = race_gnome
			}
		}
		#Gnoll
		AND = {
			$ACTOR$ = {
				has_trait = race_gnoll
			}
			$RECIPIENT$ = {
				has_trait = race_gnoll
			}
		}
		#Kobold
		AND = {
			$ACTOR$ = {
				has_trait = race_kobold
			}
			$RECIPIENT$ = {
				has_trait = race_kobold
			}
		}
		#Troll
		AND = {
			$ACTOR$ = {
				has_trait = race_troll
			}
			$RECIPIENT$ = {
				has_trait = race_troll
			}
		}
		#Ogre
		AND = {
			$ACTOR$ = {
				has_trait = race_ogre
			}
			$RECIPIENT$ = {
				has_trait = race_ogre
			}
		}
		#Goblin
		AND = {
			$ACTOR$ = {
				has_trait = race_goblin
			}
			$RECIPIENT$ = {
				OR = {
					has_trait = race_goblin
					has_trait = race_harpy
				}
			}
		}
		#Hobgoblin
		AND = {
			$ACTOR$ = {
				has_trait = race_hobgoblin
			}
			$RECIPIENT$ = {
				OR = {
					has_trait = race_hobgoblin
					has_trait = race_harpy
				}
			}
		}
		#Harimari
		AND = {
			$ACTOR$ = {
				has_trait = race_harimari
			}
			$RECIPIENT$ = {
				has_trait = race_harimari
			}
		}
		#Centaur
		AND = {
			$ACTOR$ = {
				has_trait = race_centaur
			}
			$RECIPIENT$ = {
				has_trait = race_centaur
			}
		}
		#Harpy
		AND = {
			$ACTOR$ = {
				has_trait = race_harpy
			}
			$RECIPIENT$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
					has_trait = race_goblin
					has_trait = race_hobgoblin
				}
			}
		}
	}
}

#Elves duke and up only marry other elves
elf_duke_marriage_trigger = {
	OR = {
		#Elves lower than duke tier can marry anyone otherwise valid
		$ACTOR$ = {
			has_trait = race_elf
			highest_held_title_tier < 3
		}
		#Non-elves can only marry elves if the elf is lower than duke tier
		AND = {
			$ACTOR$ = {
				NOT = { has_trait = race_elf }
			}
			$RECIPIENT$ = {
				OR = {
					NOT = { has_trait = race_elf }
					highest_held_title_tier < 3
				}
			}
		}
		#Elves duke tier or higher can only marry elves
		AND = {
			$ACTOR$ = {
				has_trait = race_elf
				highest_held_title_tier >= 3
			}
			$RECIPIENT$ = {
				has_trait = race_elf
			}
		}
	}
}

#Elves under liege with elven succession law can only marry other elves
elf_succession_marriage_trigger = {
	OR = {
		#Elves not under liege with elven succession law can marry anyone otherwise valid
		$ACTOR$ = {
			has_trait = race_elf
			NOR = {
				AND = {
					exists = liege
					liege.primary_title = {
						has_title_law = elven_elective_succession_law
					}
				}
				AND = {
					exists = top_liege
					top_liege.primary_title = {
						has_title_law = elven_elective_succession_law
					}
				}
			}
		}
		#Non-elves can only marry elves if elf is not under liege with elven succession law
		AND = {
			$ACTOR$ = {
				NOT = { has_trait = race_elf }
			}
			$RECIPIENT$ = {
				OR = {
					NOT = { has_trait = race_elf }
					NOR = {
						AND = {
							exists = liege
							liege.primary_title = {
								has_title_law = elven_elective_succession_law
							}
						}
						AND = {
							exists = top_liege
							top_liege.primary_title = {
								has_title_law = elven_elective_succession_law
							}
						}
					}
				}
			}
		}
		#Elves under liege with elven succession law can only marry other elves
		AND = {
			$ACTOR$ = {
				has_trait = race_elf
				OR = {
					AND = {
						exists = liege
						liege.primary_title = {
							has_title_law = elven_elective_succession_law
						}
					}
					AND = {
						exists = top_liege
						top_liege.primary_title = {
							has_title_law = elven_elective_succession_law
						}
					}
				}
			}
			$RECIPIENT$ = {
				has_trait = race_elf
			}
		}
	}
}

#Sun elves only marry other elves
sun_elf_marriage_trigger = {
	OR = {
		#Elves that aren't sun elves can marry anyone otherwise valid
		$ACTOR$ = {
			has_trait = race_elf
			NOT = { has_culture = culture:sun_elvish }
		}
		#Non-elves can't marry sun elves
		AND = {
			$ACTOR$ = {
				NOT = { has_trait = race_elf }
			}
			$RECIPIENT$ = {
				NAND = {
					has_trait = race_elf
					has_culture = culture:sun_elvish
				}
			}
		}
		#Sun elves can only marry other elves
		AND = {
			$ACTOR$ = {
				has_trait = race_elf
				has_culture = culture:sun_elvish
			}
			$RECIPIENT$ = {
				has_trait = race_elf
			}
		}
	}
}

#Two characters can have children even mules
#For use with pregnancy
compatible_race_trigger = {
	OR = {
		#Elf-human-orc group
		AND = {
			$MOTHER$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
				}
			}
			$FATHER$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
					has_trait = race_goblin
					has_trait = race_hobgoblin
				}
			}
		}
		#Dwarf
		AND = {
			$MOTHER$ = {
				has_trait = race_dwarf
			}
			$FATHER$ = {
				has_trait = race_dwarf
			}
		}
		#Halfling
		AND = {
			$MOTHER$ = {
				has_trait = race_halfling
			}
			$FATHER$ = {
				OR = {
					has_trait = race_gnome
					has_trait = race_halfling
				}
			}
		}
		#Gnome
		AND = {
			$MOTHER$ = {
				has_trait = race_gnome
			}
			$FATHER$ = {
				OR = {
					has_trait = race_gnome
					has_trait = race_halfling
				}
			}
		}
		#Gnoll
		AND = {
			$MOTHER$ = {
				has_trait = race_gnoll
			}
			$FATHER$ = {
				has_trait = race_gnoll
			}
		}
		#Kobold
		AND = {
			$MOTHER$ = {
				has_trait = race_kobold
			}
			$FATHER$ = {
				has_trait = race_kobold
			}
		}
		#Troll
		AND = {
			$MOTHER$ = {
				has_trait = race_troll
			}
			$FATHER$ = {
				OR = {
					has_trait = race_troll
					has_trait = race_ogre
				}
			}
		}
		#Ogre
		AND = {
			$MOTHER$ = {
				has_trait = race_ogre
			}
			$FATHER$ = {
				OR = {
					has_trait = race_troll
					has_trait = race_ogre
				}
			}
		}
		#Goblin
		AND = {
			$MOTHER$ = {
				has_trait = race_goblin
			}
			$FATHER$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
					has_trait = race_goblin
					has_trait = race_hobgoblin
				}
			}
		}
		#Hobgoblin
		AND = {
			$MOTHER$ = {
				has_trait = race_hobgoblin
			}
			$FATHER$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
					has_trait = race_goblin
					has_trait = race_hobgoblin
				}
			}
		}
		#Harimari
		AND = {
			$MOTHER$ = {
				has_trait = race_harimari
			}
			$FATHER$ = {
				has_trait = race_harimari
			}
		}
		#Centaur
		AND = {
			$MOTHER$ = {
				has_trait = race_centaur
			}
			$FATHER$ = {
				has_trait = race_centaur
			}
		}
		#Harpy
		AND = {
			$MOTHER$ = {
				has_trait = race_harpy
			}
			$FATHER$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
					has_trait = race_goblin
					has_trait = race_hobgoblin
				}
			}
		}
		#Light-mule races, only females can have children
		#Lesser Hobgoblin
		AND = {
			$MOTHER$ = {
				has_trait = race_lesser_hobgoblin
			}
			$FATHER$ = {
				OR = {
					has_trait = race_goblin
					has_trait = race_hobgoblin
				}
			}
		}
		#race_ogrillon
		AND = {
			$MOTHER$ = {
				has_trait = race_ogrillon
			}
			$FATHER$ = {
				OR = {
					has_trait = race_orc
					has_trait = race_ogre
				}
			}
		}
	}
}

#Checks if character has a racial trait
has_racial_trait = {
	OR = {
		has_trait = race_elf
		has_trait = race_half_elf
		has_trait = race_human
		has_trait = race_orc
		has_trait = race_half_orc
		has_trait = race_dwarf
		has_trait = race_halfling
		has_trait = race_gnome
		has_trait = race_gnomeling
		has_trait = race_gnoll
		has_trait = race_kobold
		has_trait = race_troll
		has_trait = race_trollkin
		has_trait = race_ogre
		has_trait = race_ogrillon
		has_trait = race_goblin
		has_trait = race_half_goblin
		has_trait = race_hobgoblin
		has_trait = race_half_hobgoblin
		has_trait = race_lesser_hobgoblin
		has_trait = race_harimari
		has_trait = race_centaur
		has_trait = race_harpy
	}
}

# Culture #

anb_is_same_racial_culture = {
	OR = {
		AND = {
			is_human_culture = yes
			$TARGET$ = { is_human_culture = yes }
		}
		AND = {
			is_elvish_culture = yes
			$TARGET$ = { is_elvish_culture = yes }
		}
		AND = {
			is_dwarvish_culture = yes
			$TARGET$ = { is_dwarvish_culture = yes }
		}
		AND = {
			is_halfling_culture = yes
			$TARGET$ = { is_halfling_culture = yes }
		}
		AND = {
			is_gnomish_culture = yes
			$TARGET$ = { is_gnomish_culture = yes }
		}
	}
}

#Checks if the scope's culture is elven
is_elvish_culture = {
	culture = { has_cultural_pillar = heritage_elven }
}

#Checks if the scope's culture is human
is_human_culture = {
	OR = {
		culture = { has_cultural_pillar = heritage_alenic }
		culture = { has_cultural_pillar = heritage_dostanorian }
		culture = { has_cultural_pillar = heritage_escanni }
		culture = { has_cultural_pillar = heritage_gerudian }
		culture = { has_cultural_pillar = heritage_lencori }
		culture = { has_cultural_pillar = heritage_damesheader }
		culture = { has_cultural_pillar = heritage_businori }
		culture = { has_cultural_pillar = heritage_bulwari }
		culture = { has_cultural_pillar = heritage_kheteratan }
		culture = { has_cultural_pillar = heritage_akani }
	}
}


# #Checks if the scope's culture is orcish
# is_orcish_culture = {
	# culture = { has_cultural_pillar = heritage_orcish }
# }

#Checks if the scope's culture is dwarven
is_dwarvish_culture = {
	culture = { has_cultural_pillar = heritage_dwarven }
}

#Checks if the scope's culture is race_halfling
is_halfling_culture = {
	culture = { has_cultural_pillar = heritage_halfling }
}

#Checks if the scope's culture is gnomish
is_gnomish_culture = {
	culture = { has_cultural_pillar = heritage_gnomish }
}

# #Checks if the scope's culture is race_gnoll
# is_gnoll_culture = {
	# culture = { has_cultural_pillar = heritage_gnoll }
# }

# #Checks if the scope's culture is race_kobold
# is_kobold_culture = {
	# culture = { has_cultural_pillar = heritage_kobold }
# }

# #Checks if the scope's culture is race_troll
# is_troll_culture = {
	# culture = { has_cultural_pillar = heritage_troll }
# }

# #Checks if the scope's culture is race_ogre
# is_ogre_culture = {
	# culture = { has_cultural_pillar = heritage_ogre }
# }

# #Checks if the scope's culture is goblin
# is_goblin_culture = {
	# culture = { has_cultural_pillar = heritage_goblin }
# }

# #Checks if the scope's culture is hobgoblin
# is_hobgoblin_culture = {
	# culture = { has_cultural_pillar = heritage_hobgoblin }
# }

# #Checks if the scope's culture is race_harimari
# is_harimari_culture = {
	# culture = { has_cultural_pillar = heritage_harimari }
# }

# #Checks if the scope's culture is race_centaur
# is_centaur_culture = {
	# culture = { has_cultural_pillar = heritage_centaur }
# }

# #Checks if the scope's culture is race_harpy
# is_harpy_culture = {
	# culture = { has_cultural_pillar = heritage_harpy }
# }

#Checks if father and real father can result in same races
assumed_father_matches_child_race = {
	OR = {
		AND = {
			$MOTHER$ = {
				has_trait = race_elf
			}
			$FATHER$ = {
				has_trait = race_elf
			}
			$CHILD$ = {
				has_trait = race_elf
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_half_elf
			}
			$FATHER$ = {
				has_trait = race_half_elf
			}
			$CHILD$ = {
				has_trait = race_half_elf
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_human
			}
			$FATHER$ = {
				has_trait = race_human
			}
			$CHILD$ = {
				has_trait = race_human
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_orc
			}
			$FATHER$ = {
				has_trait = race_orc
			}
			$CHILD$ = {
				has_trait = race_orc
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_half_orc
			}
			$FATHER$ = {
				has_trait = race_half_orc
			}
			$CHILD$ = {
				has_trait = race_half_orc
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_elf
			}
			$FATHER$ = {
				OR = {
					has_trait = race_human
					has_trait = race_half_elf
					has_trait = race_half_orc
				}
			}
			$CHILD$ = {
				has_trait = race_half_elf
			}
		}
		AND = {
			$MOTHER$ = {
				OR = {
					has_trait = race_human
					has_trait = race_half_elf
					has_trait = race_half_orc
				}
			}
			$FATHER$ = {
				has_trait = race_elf
			}
			$CHILD$ = {
				has_trait = race_half_elf
			}
		}
		AND = {
			$MOTHER$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_half_orc
				}
			}
			$FATHER$ = {
				has_trait = race_orc
			}
			$CHILD$ = {
				has_trait = race_half_orc
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_orc
			}
			$FATHER$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_half_orc
				}
			}
			$CHILD$ = {
				has_trait = race_half_orc
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_half_elf
			}
			$FATHER$ = {
				has_trait = race_human
			}
			$CHILD$ = {
				OR = {
					has_trait = race_half_elf
					has_trait = race_human
				}
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_human
			}
			$FATHER$ = {
				has_trait = race_half_elf
			}
			$CHILD$ = {
				OR = {
					has_trait = race_half_elf
					has_trait = race_human
				}
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_half_orc
			}
			$FATHER$ = {
				has_trait = race_human
			}
			$CHILD$ = {
				OR = {
					has_trait = race_half_orc
					has_trait = race_human
				}
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_human
			}
			$FATHER$ = {
				has_trait = race_half_orc
			}
			$CHILD$ = {
				OR = {
					has_trait = race_half_orc
					has_trait = race_human
				}
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_half_elf
			}
			$FATHER$ = {
				has_trait = race_half_orc
			}
			$CHILD$ = {
				OR = {
					has_trait = race_half_elf
					has_trait = race_half_orc
				}
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_half_orc
			}
			$FATHER$ = {
				has_trait = race_half_elf
			}
			$CHILD$ = {
				OR = {
					has_trait = race_half_elf
					has_trait = race_half_orc
				}
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_dwarf
			}
			$FATHER$ = {
				has_trait = race_dwarf
			}
			$CHILD$ = {
				has_trait = race_dwarf
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_halfling
			}
			$FATHER$ = {
				has_trait = race_halfling
			}
			$CHILD$ = {
				has_trait = race_halfling
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_gnome
			}
			$FATHER$ = {
				has_trait = race_gnome
			}
			$CHILD$ = {
				has_trait = race_gnome
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_gnoll
			}
			$FATHER$ = {
				has_trait = race_gnoll
			}
			$CHILD$ = {
				has_trait = race_gnoll
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_kobold
			}
			$FATHER$ = {
				has_trait = race_kobold
			}
			$CHILD$ = {
				has_trait = race_kobold
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_troll
			}
			$FATHER$ = {
				has_trait = race_troll
			}
			$CHILD$ = {
				has_trait = race_troll
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_ogre
			}
			$FATHER$ = {
				has_trait = race_ogre
			}
			$CHILD$ = {
				has_trait = race_ogre
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_goblin
			}
			$FATHER$ = {
				has_trait = race_goblin
			}
			$CHILD$ = {
				has_trait = race_goblin
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_hobgoblin
			}
			$FATHER$ = {
				has_trait = race_hobgoblin
			}
			$CHILD$ = {
				has_trait = race_hobgoblin
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_harimari
			}
			$FATHER$ = {
				has_trait = race_harimari
			}
			$CHILD$ = {
				has_trait = race_harimari
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_centaur
			}
			$FATHER$ = {
				has_trait = race_centaur
			}
			$CHILD$ = {
				has_trait = race_centaur
			}
		}
		AND = {
			$MOTHER$ = {
				has_trait = race_harpy
			}
			$FATHER$ = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
					has_trait = race_goblin
					has_trait = race_hobgoblin
				}
			}
			$CHILD$ = {
				has_trait = race_harpy
			}
		}
	}
}

anb_same_race_trigger = {
	OR = {
		AND = {
			$ACTOR$ = {
				has_trait = race_elf
			}
			$RECIPIENT$ = {
				has_trait = race_elf
			}
		}
		AND = {
			$ACTOR$ = {
				has_trait = race_half_elf
			}
			$RECIPIENT$ = {
				has_trait = race_half_elf
			}
		}
		AND = {
			$ACTOR$ = {
				has_trait = race_human
			}
			$RECIPIENT$ = {
				has_trait = race_human
			}
		}
		AND = {
			$ACTOR$ = {
				has_trait = race_dwarf
			}
			$RECIPIENT$ = {
				has_trait = race_dwarf
			}
		}
		AND = {
			$ACTOR$ = {
				has_trait = race_gnome
			}
			$RECIPIENT$ = {
				has_trait = race_gnome
			}
		}
		AND = {
			$ACTOR$ = {
				has_trait = race_halfling
			}
			$RECIPIENT$ = {
				has_trait = race_halfling
			}
		}
	}
}

anb_is_racial_purist_trigger = {
	OR = {
		has_trait = human_purist
		has_trait = gnomish_purist
		has_trait = elven_purist
		has_trait = halfling_purist
		has_trait = half_elf_purist
		has_trait = dwarven_purist
	}
}

race_has_400_year_lifespan = {
	has_trait = race_elf
}

race_has_250_year_lifespan = {
	OR = {
		has_trait = race_gnomeling
		has_trait = race_gnome
	}
}

race_has_200_year_lifespan = {
	has_trait = race_dwarf
}

race_has_100_year_lifespan = {
	OR = {
		has_trait = race_half_elf
		has_trait = race_kobold
		has_trait = race_troll
		has_trait = race_trollkin
		has_trait = race_ogre
		has_trait = race_ogrillon
		has_trait = race_harimari
	}
}

race_has_50_year_lifespan = {
	OR = {
		has_trait = race_gnoll
		has_trait = race_harpy
	}
}

race_has_average_year_lifespan = {
	OR = {
		has_trait = race_human
		has_trait = race_orc
		has_trait = race_half_orc
		has_trait = race_goblin
		has_trait = race_half_goblin
		has_trait = race_hobgoblin
		has_trait = race_half_hobgoblin
		has_trait = race_lesser_hobgoblin
		has_trait = race_centaur
	}
}

is_monstrous_race = {
	OR = {
		has_trait = race_centaur
		has_trait = race_gnoll
		has_trait = race_goblin
		has_trait = race_half_goblin
		has_trait = race_half_hobgoblin
		has_trait = race_harpy
		has_trait = race_hobgoblin
		has_trait = race_kobold
		has_trait = race_ogre
		has_trait = race_ogrillon
		has_trait = race_orc
		has_trait = race_troll
		has_trait = race_trollkin
	}
}

is_civilized_race = {
	NOT = { is_monstrous_race = yes }
}

is_part_human = {
	OR = {
		has_trait = race_human
		has_trait = race_half_elf
		has_trait = race_half_orc
	}
}

character_is_illegitimate_race_for_title = {
	custom_description = {
		text = is_illegitimate_race_trigger
		$TITLE$ = {
			OR = {
				any_this_title_or_de_jure_above = {
					# Checks the holder is the de-factor liege or is the character
					OR = {
						is_de_facto_liege_or_above_target = $TITLE$
						AND = {
							exists = holder
							holder = { has_title = $TITLE$ }
						}
					}
					NOT = {
						character_is_legitimate_race_for_title_laws = { 
							CHARACTER = $CHARACTER$
							TITLE = this
						}
					}
				}
			}
		}
	}
}

# Checks if the laws allow a character to hold that title, does NOT check dejure
character_is_legitimate_race_for_title_laws = {
	custom_description = {
		text = is_legitimate_race_trigger
		$TITLE$ = {
			NOR = {
				AND = {
					has_title_law = elf_only
					NOT = { $CHARACTER$ = { has_trait = race_elf } }
				}
				AND = {
					has_title_law = half_elf_only
					NOT = { $CHARACTER$ = { has_trait = race_half_elf } }
				}
				AND = {
					has_title_law = human_only
					NOT = { $CHARACTER$ = { has_trait = race_human } }
				}
				AND = {
					has_title_law = dwarf_only
					NOT = { $CHARACTER$ = { has_trait = race_dwarf } }
				}
				AND = {
					has_title_law = halfling_only
					NOT = { $CHARACTER$ = { has_trait = race_halfling } }
				}
				AND = {
					has_title_law = gnome_only
					NOT = { $CHARACTER$ = { has_trait = race_gnome } }
				}
				AND = {
					has_title_law = gnomeling_only
					NOT = { $CHARACTER$ = { has_trait = race_gnomeling } }
				}
				AND = {
					has_title_law = orc_only
					NOT = { $CHARACTER$ = { has_trait = race_orc } }
				}
				AND = {
					has_title_law = half_orc_only
					NOT = { $CHARACTER$ = { has_trait = race_half_orc } }
				}
				AND = {
					has_title_law = gnoll_only
					NOT = { $CHARACTER$ = { has_trait = race_gnoll } }
				}
				AND = {
					has_title_law = kobold_only
					NOT = { $CHARACTER$ = { has_trait = race_kobold } }
				}
				AND = {
					has_title_law = troll_only
					NOT = { $CHARACTER$ = { has_trait = race_troll } }
				}
				AND = {
					has_title_law = trollkin_only
					NOT = { $CHARACTER$ = { has_trait = race_trollkin } }
				}
				AND = {
					has_title_law = ogre_only
					NOT = { $CHARACTER$ = { has_trait = race_ogre } }
				}
				AND = {
					has_title_law = ogrillon_only
					NOT = { $CHARACTER$ = { has_trait = race_ogrillon } }
				}
				AND = {
					has_title_law = goblin_only
					NOT = { $CHARACTER$ = { has_trait = race_goblin } }
				}
				AND = {
					has_title_law = half_goblin_only
					NOT = { $CHARACTER$ = { has_trait = race_half_goblin } }
				}
				AND = {
					has_title_law = hobgoblin_only
					NOT = { $CHARACTER$ = { has_trait = race_hobgoblin } }
				}
				AND = {
					has_title_law = half_hobgoblin_only
					NOT = { $CHARACTER$ = { has_trait = race_half_hobgoblin } }
				}
				AND = {
					has_title_law = lesser_hobgoblin_only
					NOT = { $CHARACTER$ = { has_trait = race_lesser_hobgoblin } }
				}
				AND = {
					has_title_law = harimari_only
					NOT = { $CHARACTER$ = { has_trait = race_harimari } }
				}
				AND = {
					has_title_law = centaur_only
					NOT = { $CHARACTER$ = { has_trait = race_centaur } }
				}
				AND = {
					has_title_law = harpy_only
					NOT = { $CHARACTER$ = { has_trait = race_harpy } }
				}
				AND = {
					has_title_law = human_plus_halves_only
					NOT = { $CHARACTER$ = { is_part_human = yes } }
				}
				AND = {
					has_title_law = all_civilized_only
					$CHARACTER$ = { is_monstrous_race = yes }
				}
				AND = {
					has_title_law = all_monstrous_only
					NOT = { $CHARACTER$ = { is_monstrous_race = yes } }
				}
			}
		}
	}
}

can_change_single_racial_law_trigger = {
	can_change_racial_law_trigger = yes
	has_trait = race_$RACE$
	custom_description = {
		text = all_powerful_vassals_must_be_legitimate_trigger
		NOT = {
			any_vassal = {
				is_powerful_vassal = yes
				NOT = { has_trait = race_$RACE$ }
			}
		}
	}
}

can_change_group_racial_law_trigger = {
	can_change_racial_law_trigger = yes
	is_$GROUP$_race = yes
	custom_description = {
		text = all_powerful_vassals_must_be_legitimate_trigger
		NOT = {
			any_vassal = {
				is_powerful_vassal = yes
				NOT = { is_$GROUP$_race = yes }
			}
		}
	}
}

can_have_single_racial_law_trigger = {
	can_have_racial_law_trigger = yes
	OR = {
		holder = {
			has_trait = race_$RACE$
		}
		has_title_law = $RACE$_only
	}
}

can_have_multi_racial_law_trigger = {
	can_have_racial_law_trigger = yes
	OR = {
		holder = {
			is_$GROUP$_race = yes
		}
		has_title_law = all_$GROUP$_only
	}
}

can_change_racial_law_trigger = {
	can_change_title_law_trigger = yes
}

can_have_racial_law_trigger = {
	tier >= tier_duchy
	has_game_rule = legitimacy_and_supremacy_on
	is_temporal_head_of_faith_trigger = no # Can't lock your head of faith to one race only, as it does not have dejure
}

character_can_get_purist_trait_for_race = {
	has_game_rule = legitimacy_and_supremacy_on
	has_trait = race_$RACE$
}
