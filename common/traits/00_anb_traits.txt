﻿########## Racial Traits ##########

#Elf - If you updat this remember to update race_elf_dead
race_elf = {
	index = 300
	
	fertility = -0.5	#was 0.75
	monthly_lifestyle_xp_gain_mult = -0.75
	years_of_fertility = 220	#so maybe like 250 it falls off	#45+ will give x0.1 multiplier so its the cutoff. 40 is 0.33, 35 is 0.5
	life_expectancy = 350
	no_prowess_loss_from_age = yes
	negate_health_penalty_add = 1
	
	martial = 1
	intrigue = 1
	stewardship = 1
	learning = 1
	diplomacy = 1

	#health = 0.75
	
	attraction_opinion = 20
	
	same_opinion = 5
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0

	#forced_portrait_age_index = 6 # old_beauty_1	#no aging mate	#this does fuck all honestly. the true answer is portrait_modifiers
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = consecrated_blood.dds
			}
			desc = consecrated_blood.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_elf_desc
			}
			desc = trait_race_elf_character_desc
		}
	}
}

#Half-Elf
race_half_elf = {
	index = 301
	
	diplomacy = 2

	life_expectancy = 30

	attraction_opinion = 10
	
	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	#genetic_constraint_all = beauty_2
	#genetic_constraint_men = male_beauty_2
	#genetic_constraint_women = female_beauty_2
	#forced_portrait_age_index = 1 # old_beauty_1

	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = blood_of_prophet.dds
			}
			desc = blood_of_prophet.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_half_elf_desc
			}
			desc = trait_race_half_elf_character_desc
		}
	}
}

# #Elf Blooded
# elf_blood = {
	# index = 302
	
	# fertility -0.1
	# learning = 2
	# diplomacy = 2
	
	# attraction_opinion = 10
	
	# vassal_opinion = 5
	# same_opinion = 10
	
	# inherit_chance = 100
	# both_parent_has_trait_inherit_chance = 100
	# physical = yes
	
	# birth = 0
	# random_creation = 0
	
	# icon = {
		# first_valid = {
			# triggered_desc = {
				# trigger = { NOT = { exists = this } }
				# desc = blood_of_the_prophet_parent.dds
			# }
			# desc = blood_of_the_prophet_parent.dds
		# }
	# }	
	# desc = {
		# first_valid = {
			# triggered_desc = {
				# trigger = {
					# NOT = { exists = this }
				# }
				# desc = trait_race_elf_blood_desc
			# }
			# desc = trait_race_elf_blood_character_desc
		# }
	# }
# }

#Human
race_human = {
	index = 303
	
	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_human_desc
			}
			desc = trait_race_human_character_desc
		}
	}
}

#Dwarf
race_dwarf = {
	index = 304
	
	fertility = -0.25
	monthly_lifestyle_xp_gain_mult = -0.5
	#years_of_fertility = 25
	years_of_fertility = 75	#120
	life_expectancy = 150
	no_prowess_loss_from_age = yes
	negate_health_penalty_add = 0.5
	
	stewardship = 1
	prowess = 3

	same_opinion = 5
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = sea_raider.dds
			}
			desc = sea_raider.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_dwarf_desc
			}
			desc = trait_race_dwarf_character_desc
		}
	}
}

#Halfling
race_halfling = {
	index = 305
	
	fertility = 0.5
	prowess = -3

	same_opinion = 5
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = dwarf.dds
			}
			desc = dwarf.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_halfling_desc
			}
			desc = trait_race_halfling_character_desc
		}
	}
}

#Gnome
race_gnome = {
	index = 306
	
	fertility = -0.5
	monthly_lifestyle_xp_gain_mult = -0.66
	#years_of_fertility = 35
	years_of_fertility = 100
	life_expectancy = 200
	no_prowess_loss_from_age = yes
	negate_health_penalty_add = 0.5
	
	learning = 1
	prowess = -5

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = bossy.dds
			}
			desc = bossy.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_gnome_desc
			}
			desc = trait_race_gnome_character_desc
		}
	}
}

#Gnomling
race_gnomeling = {
	index = 307

	life_expectancy = 150	#from gnomish blood
	no_prowess_loss_from_age = yes
	
	can_have_children = no
	learning = 1
	prowess = -3

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = bossy.dds
			}
			desc = bossy.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_gnomeling_desc
			}
			desc = trait_race_gnomeling_character_desc
		}
	}
}

#Orc
race_orc = {
	index = 308
	
	prowess = 5

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_orc_desc
			}
			desc = trait_race_orc_character_desc
		}
	}
}

#Half-Orc
race_half_orc = {
	index = 309
	
	martial = 1
	prowess = 3

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_half_orc_desc
			}
			desc = trait_race_half_orc_character_desc
		}
	}
}

#Gnoll
race_gnoll = {
	index = 310
	
	fertility = 0.25
	prowess = 5
	life_expectancy = -20	#80 - 30 = 60

	same_opinion = 8
	general_opinion = -5
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_gnoll_desc
			}
			desc = trait_race_gnoll_character_desc
		}
	}
}

#Kobold
race_kobold = {
	index = 311
	
	health = -0.5
	fertility = 0.75
	prowess = -3
	life_expectancy = 30

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_kobold_desc
			}
			desc = trait_race_kobold_character_desc
		}
	}
}

#Troll
race_troll = {
	index = 312
	
	fertility = -0.5
	health = 0.5	#regeneration
	prowess = 10
	life_expectancy = 20	#100

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_troll_desc
			}
			desc = trait_race_troll_character_desc
		}
	}
}

#Trollkin
race_trollkin = {
	index = 313
	
	can_have_children = no

	health = 0.25	#regeneration
	prowess = 5
	life_expectancy = 20	#100

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_trollkin_desc
			}
			desc = trait_race_trollkin_character_desc
		}
	}
}

#Ogre
race_ogre = {
	index = 314
	
	fertility = -0.25
	prowess = 10
	life_expectancy = 20	#100

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_ogre_desc
			}
			desc = trait_race_ogre_character_desc
		}
	}
}

#race_ogrillon
race_ogrillon = {
	index = 315
	
	fertility = -0.5
	life_expectancy = 20	#100

	prowess = 7

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_ogrillon_desc
			}
			desc = trait_race_ogrillon_character_desc
		}
	}
}

#Goblin
race_goblin = {
	index = 316
	
	health = -0.25
	fertility = 0.5

	intrigue = 1
	
	prowess = -3

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_goblin_desc
			}
			desc = trait_race_goblin_character_desc
		}
	}
}

#Half-goblin
race_half_goblin = {
	index = 317
	
	can_have_children = no
	intrigue = 1
	prowess = -2

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_half_goblin_desc
			}
			desc = trait_race_half_goblin_character_desc
		}
	}
}

#Hobgoblin
race_hobgoblin = {
	index = 318
	
	martial = 1
	prowess = 7

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_hobgoblin_desc
			}
			desc = trait_race_hobgoblin_character_desc
		}
	}
}

#Half-hobgoblin
race_half_hobgoblin = {
	index = 319
	
	can_have_children = no
	martial = 1
	prowess = 5

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_half_hobgoblin_desc
			}
			desc = trait_race_half_hobgoblin_character_desc
		}
	}
}

#Lesser Hobgoblin
race_lesser_hobgoblin = {
	index = 320
	
	fertility = -0.5
	prowess = 2

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_lesser_hobgoblin_desc
			}
			desc = trait_race_lesser_hobgoblin_character_desc
		}
	}
}

#Harimari
race_harimari = {
	index = 321
	
	fertility = -0.25
	martial = 1
	learning = 1
	prowess = 5
	life_expectancy = 30

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_harimari_desc
			}
			desc = trait_race_harimari_character_desc
		}
	}
}

#Centaur
race_centaur = {
	index = 322
	
	prowess = 7

	same_opinion = 2
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_centaur_desc
			}
			desc = trait_race_centaur_character_desc
		}
	}
}

#Harpy
race_harpy = {
	index = 323
	
	fertility = 0.25	#Since half of pregnancies will end early due to being boys - #used to be 1 but removed this as we dont need it anymore?

	same_opinion = 2
	life_expectancy = -20	#80 - 30 = 60
	
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = whole_of_body.dds
			}
			desc = whole_of_body.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_harpy_desc
			}
			desc = trait_race_harpy_character_desc
		}
	}
}

########## Magic System Traits ##########

magical_affinity_1 = {
	index = 324

	attraction_opinion = 5
	monthly_magic_lifestyle_xp_gain_mult = 0.1
	fertility = -0.05

	birth = 0.1
	random_creation = 0.1

	group = magical_affinity
	level = 1

	genetic = yes
	good = yes
	physical = yes

	# icon = {
	# 	desc = x.dds
	# }

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_magical_affinity_1_desc
			}
			desc = trait_magical_affinity_1_character_desc
		}
	}
}

magical_affinity_2 = {
	index = 325

	attraction_opinion = 15
	monthly_magic_lifestyle_xp_gain_mult = 0.2
	fertility = -0.1

	birth = 0.05
	random_creation = 0.05

	group = magical_affinity
	level = 2

	genetic = yes
	good = yes
	physical = yes

	# icon = {
	# 	desc = x.dds
	# }

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_magical_affinity_2_desc
			}
			desc = trait_magical_affinity_2_character_desc
		}
	}
}

magical_affinity_3 = {
	index = 326

	attraction_opinion = 25
	monthly_magic_lifestyle_xp_gain_mult = 0.3 
	fertility = -0.15	#being magical has some complications

	birth = 0.01
	random_creation = 0.01

	group = magical_affinity
	level = 3

	genetic = yes
	good = yes
	physical = yes

	# icon = {
	# 	desc = x.dds
	# }

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_magical_affinity_3_desc
			}
			desc = trait_magical_affinity_3_character_desc
		}
	}
}

forbidden_magic_practitioner = {
	index = 327

	dread_baseline_add = 20
	general_opinion = -10				#Feared/Distrusted even when culturally/religiously accepted - Subject to Change
	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_forbidden_magic_practitioner_desc
			}
			desc = trait_forbidden_magic_practitioner_character_desc
		}
	}

	ai_compassion = very_high_negative_ai_value
	ai_boldness = low_positive_ai_value
	ai_rationality = low_positive_ai_value
	
	triggered_opinion = {
		parameter = witchcraft_illegal
		opinion_modifier = forbidden_magic_crime
		ignore_opinion_value_if_same_trait = yes
	}
	triggered_opinion = {
		parameter = witchcraft_shunned
		opinion_modifier = forbidden_magic_intolerant
		ignore_opinion_value_if_same_trait = yes
	}
}

#### RELIGION ####
# Jaher Himself
sun_reborn = {
	index = 328
	fame = yes
	opposites = {
		sun_reborn_descendant
	}
	diplomacy = 1
	learning = 2
	prowess = 2

	same_faith_opinion = 5
	
	shown_in_ruler_designer = no

	ai_zeal = 100
}

# Jaher's Heirs of the Body
sun_reborn_descendant = {
	index = 329
	fame = yes
	opposites = {
		sun_reborn
	}
	learning = 1

	same_faith_opinion = 5
	
	shown_in_ruler_designer = no
	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_sun_reborn_descendant_desc
			}
			desc = trait_sun_reborn_descendant_character_desc
		}
	}
}

race_elf_dead = {
	index = 330
	
	fertility = -0.5	#was 0.75
	monthly_lifestyle_xp_gain_mult = -0.75
	years_of_fertility = 220	#so maybe like 250 it falls off	#45+ will give x0.1 multiplier so its the cutoff. 40 is 0.33, 35 is 0.5
	life_expectancy = 350
	no_prowess_loss_from_age = yes
	negate_health_penalty_add = 1

	immortal = yes
	
	martial = 1
	intrigue = 1
	stewardship = 1
	learning = 1
	diplomacy = 1

	#health = 0.75
	
	attraction_opinion = 20
	
	same_opinion = 5
	
	inherit_chance = 100
	both_parent_has_trait_inherit_chance = 100
	physical = yes
	
	birth = 0
	random_creation = 0
	
	# potential = {
		# is_alive = no
	# }

	#forced_portrait_age_index = 6 # old_beauty_1	#no aging mate	#this does fuck all honestly. the true answer is portrait_modifiers
	
	name = trait_race_elf
	icon = {
		first_valid = {
			triggered_desc = {
				trigger = { NOT = { exists = this } }
				desc = consecrated_blood.dds
			}
			desc = consecrated_blood.dds
		}
	}	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_race_elf_desc
			}
			desc = trait_race_elf_character_desc
		}
	}
}

########## Racial Attitude Traits ##########
elven_purist = {
	index = 400
	flag = racial_purist
	random_creation = 5

	opposites = {
		race_human
		race_halfling
		race_dwarf
		race_gnome
		race_half_elf
	}

	potential = {
		character_can_get_purist_trait_for_race = {
			RACE = elf
		}
	}

	opposite_opinion = -15
	same_opinion = 10

	compatibility = {
		elven_purist = @pos_compat_high
		human_purist = @neg_compat_high
		gnomish_purist = @neg_compat_high
		dwarven_purist = @neg_compat_high
		half_elf_purist = @neg_compat_high
		halfling_purist = @neg_compat_high

		race_elf = @pos_compat_medium
		race_human = @neg_compat_medium
		race_gnome = @neg_compat_medium
		race_dwarf = @neg_compat_medium
		race_half_elf = @neg_compat_medium
		race_halfling = @neg_compat_medium
	}
	# icon = {
	# 	first_valid = {
	# 		triggered_desc = {
	# 			trigger = { NOT = { exists = this } }
	# 			desc = X.dds
	# 		}
	# 		desc = X.dds
	# 	}
	# }
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_elven_purist_desc
			}
			desc = trait_elven_purist_character_desc
		}
	}
}

human_purist = {
	index = 401
	flag = racial_purist
	random_creation = 5

	opposites = {
		race_elf
		race_halfling
		race_dwarf
		race_gnome
		race_half_elf
	}
	
	potential = {
		character_can_get_purist_trait_for_race = {
			RACE = human
		}
	}

	opposite_opinion = -15
	same_opinion = 10

	compatibility = {
		elven_purist = @neg_compat_high
		human_purist = @pos_compat_high
		gnomish_purist = @neg_compat_high
		dwarven_purist = @neg_compat_high
		half_elf_purist = @neg_compat_high
		halfling_purist = @neg_compat_high

		race_elf = @neg_compat_medium
		race_human = @pos_compat_medium
		race_gnome = @neg_compat_medium
		race_dwarf = @neg_compat_medium
		race_half_elf = @neg_compat_medium
		race_halfling = @neg_compat_medium
	}
	# icon = {
	# 	first_valid = {
	# 		triggered_desc = {
	# 			trigger = { NOT = { exists = this } }
	# 			desc = X.dds
	# 		}
	# 		desc = X.dds
	# 	}
	# }	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_human_purist_desc
			}
			desc = trait_human_purist_character_desc
		}
	}
}

gnomish_purist = {
	index = 402
	flag = racial_purist
	random_creation = 5

	opposites = {
		race_elf
		race_halfling
		race_dwarf
		race_human
		race_half_elf
	}
	
	potential = {
		character_can_get_purist_trait_for_race = {
			RACE = gnome
		}
	}

	opposite_opinion = -15
	same_opinion = 10

	compatibility = {
		elven_purist = @neg_compat_high
		human_purist = @neg_compat_high
		gnomish_purist = @pos_compat_high
		dwarven_purist = @neg_compat_high
		half_elf_purist = @neg_compat_high
		halfling_purist = @neg_compat_high

		race_elf = @neg_compat_medium
		race_human = @neg_compat_medium
		race_gnome = @pos_compat_medium
		race_dwarf = @neg_compat_medium
		race_half_elf = @neg_compat_medium
		race_halfling = @neg_compat_medium
	}
	# icon = {
	# 	first_valid = {
	# 		triggered_desc = {
	# 			trigger = { NOT = { exists = this } }
	# 			desc = X.dds
	# 		}
	# 		desc = X.dds
	# 	}
	# }	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_gnomish_purist_desc
			}
			desc = trait_gnomish_purist_character_desc
		}
	}
}

dwarven_purist = {
	index = 403
	flag = racial_purist
	random_creation = 5

	opposites = {
		race_elf
		race_halfling
		race_human
		race_gnome
		race_half_elf
	}
	
	potential = {
		character_can_get_purist_trait_for_race = {
			RACE = dwarf
		}
	}

	opposite_opinion = -15
	same_opinion = 10

	compatibility = {
		elven_purist = @neg_compat_high
		human_purist = @neg_compat_high
		gnomish_purist = @neg_compat_high
		dwarven_purist = @pos_compat_high
		half_elf_purist = @neg_compat_high
		halfling_purist = @neg_compat_high

		race_elf = @neg_compat_medium
		race_human = @neg_compat_medium
		race_gnome = @neg_compat_medium
		race_dwarf = @pos_compat_medium
		race_half_elf = @neg_compat_medium
		race_halfling = @neg_compat_medium
	}
	# icon = {
	# 	first_valid = {
	# 		triggered_desc = {
	# 			trigger = { NOT = { exists = this } }
	# 			desc = X.dds
	# 		}
	# 		desc = X.dds
	# 	}
	# }	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_dwarven_purist_desc
			}
			desc = trait_dwarven_purist_character_desc
		}
	}
}

half_elf_purist = {
	index = 405
	flag = racial_purist
	random_creation = 5

	opposites = {
		race_elf
		race_halfling
		race_dwarf
		race_gnome
		race_human
	}
	
	potential = {
		character_can_get_purist_trait_for_race = {
			RACE = half_elf
		}
	}

	opposite_opinion = -15
	same_opinion = 10

	compatibility = {
		elven_purist = @neg_compat_high
		human_purist = @neg_compat_high
		gnomish_purist = @neg_compat_high
		dwarven_purist = @neg_compat_high
		half_elf_purist = @pos_compat_high
		halfling_purist = @neg_compat_high

		race_elf = @neg_compat_medium
		race_human = @neg_compat_medium
		race_gnome = @neg_compat_medium
		race_dwarf = @neg_compat_medium
		race_half_elf = @pos_compat_medium
		race_halfling = @neg_compat_medium
	}
	# icon = {
	# 	first_valid = {
	# 		triggered_desc = {
	# 			trigger = { NOT = { exists = this } }
	# 			desc = X.dds
	# 		}
	# 		desc = X.dds
	# 	}
	# }	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_half_elf_purist_desc
			}
			desc = trait_half_elf_purist_character_desc
		}
	}
}

halfling_purist = {
	index = 406
	flag = racial_purist
	random_creation = 5

	opposites = {
		race_elf
		race_dwarf
		race_gnome
		race_half_elf
		race_human
	}
	
	potential = {
		character_can_get_purist_trait_for_race = {
			RACE = halfling
		}
	}

	opposite_opinion = -15
	same_opinion = 10

	compatibility = {
		elven_purist = @neg_compat_high
		human_purist = @neg_compat_high
		gnomish_purist = @neg_compat_high
		dwarven_purist = @neg_compat_high
		half_elf_purist = @neg_compat_high
		halfling_purist = @pos_compat_high

		race_elf = @neg_compat_medium
		race_human = @neg_compat_medium
		race_gnome = @neg_compat_medium
		race_dwarf = @neg_compat_medium
		race_half_elf = @neg_compat_medium
		race_halfling = @pos_compat_medium
	}
	# icon = {
	# 	first_valid = {
	# 		triggered_desc = {
	# 			trigger = { NOT = { exists = this } }
	# 			desc = X.dds
	# 		}
	# 		desc = X.dds
	# 	}
	# }	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_halfling_purist_desc
			}
			desc = trait_halfling_purist_character_desc
		}
	}
}

bladeshunned = {
	index = 407
	random_creation = 0
	
	monthly_prestige = -1
	general_opinion = -5
	
	triggered_opinion  = {
		opinion_modifier = bladeshunned_blademarches_dejure
		
		ignore_opinion_value_if_same_trait = yes
	}
	
	potential = {
		OR = {
			NOT = { has_character_modifier = wielded_calindal }
			has_trait = blind # If you get blinded you can become bladeshunned even if you wielded calindal?
		}
	}
	
	desc = bladeshunned_trait_desc
}